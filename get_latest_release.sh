#!/bin/bash

# from https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
get_latest_release() {
  wget -O - "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}
# end from gist

wget -O source.tar.gz "https://github.com/coova/coova-chilli/archive/refs/tags/$(get_latest_release coova/coova-chilli).tar.gz"
