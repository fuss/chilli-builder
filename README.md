# FUSS Coova Chilli Builder

[![pipeline status](https://gitlab.fuss.bz.it/fuss/chilli-builder/badges/main/pipeline.svg)](https://gitlab.fuss.bz.it/fuss/chilli-builder/-/commits/main)

A repository to run CI/CD to build upstream Coova Chilli.